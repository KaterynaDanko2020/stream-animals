package org.katerynadanko;

public interface Animal {

        String getName();
        int getAge();
        String voice();

}
