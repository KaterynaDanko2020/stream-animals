package org.katerynadanko;

import java.util.*;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Person {
    private String name;
    private List<Animal> animals = new ArrayList<>();

    public Person(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public List<Animal> getAnimals() {
        return animals;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", animals=" + animals +
                '}';
    }

    public static void main(String[] args) {

        System.out.println("1: ");exampl1();
        System.out.println("2: "+exampl2());
        System.out.println("3: "+exampl3());
        System.out.println("4: "+exampl4());
        System.out.println("5: "+exampl5());
        System.out.println("6: "+exampl6());
        System.out.println("7: "+exampl7());
        System.out.println("8: "+exampl8());
        System.out.println("9: "+exampl9());
        System.out.println("10: "+exampl10());

    }

    public static Collection<Person> getPeople(){
        Person vito = new Person("Vito");
        vito.getAnimals().add(new Dog("Sharik", 3));
        vito.getAnimals().add(new Cat("Tom", 5));

        Person michael = new Person("Michael");
        michael.getAnimals().add(new Cat("Pushok", 1));
        michael.getAnimals().add(new Dog("Barsik", 7));
        michael.getAnimals().add(new Dog("Sharik", 5));

        Person sarah = new Person("Sarah");
        sarah.getAnimals().add(new Cat("Kiki", 3));
        sarah.getAnimals().add(new Cat("Tom", 2));
        sarah.getAnimals().add(new Dog("Piki", 10));
        sarah.getAnimals().add(new Dog("Tiki", 8));

        Collection<Person> people = Arrays.asList(vito, michael, sarah);
        return people;
    }
public  static void exampl1() {
    getPeople().stream()
            .map(Person::getAnimals)
            .flatMap(Collection::stream)
            .forEach(System.out::println);
}

    public  static  List exampl2() {
        return getPeople().stream()
                .map(Person::getAnimals)
                .collect(Collectors.toList());
    }

    public  static List<Animal> exampl3() {
        return getPeople().stream()
                .map(Person::getAnimals)
                .flatMap(Collection::stream)
                .filter(it -> it instanceof Dog)
                .collect(Collectors.toList());
    }

    public  static OptionalDouble exampl4() {
        return getPeople().stream()
                .map(Person::getAnimals)
                .flatMap(Collection::stream)
                .filter(it -> it instanceof Cat && it.getName() != "Tom")
                .mapToInt(Animal::getAge)
                .average();
    }


    public  static  List<String> exampl5() {

        return getPeople().stream()
                .map(Person::getAnimals)
                .flatMap(Collection::stream)
                .filter(it -> it.getAge() > 4)
                .map(Animal::voice)
                .collect(Collectors.toList());
    }

        public  static  List<Person> exampl6() {

            return getPeople().stream()
                    .filter(it -> it.getAnimals().size() > 2)
                    .collect(Collectors.toList());
        }

    public  static  List<String> exampl7() {

        return getPeople().stream()
                .map(Person::getAnimals)
                .flatMap(Collection::stream)
                .filter(it -> it instanceof Cat)
                .map(Animal::getName)
                .distinct()
                .collect(Collectors.toList());
    }

    public  static  List<String> exampl8() {

        return getPeople().stream()
                .map(Person::getAnimals)
                .flatMap(Collection::stream)
                .filter(it -> it instanceof Dog)
                .map(Animal::getName)
                .distinct()
                .map(String::toUpperCase)
                .collect(Collectors.toList());
    }

    public  static  Map<Object, Object> exampl9() {

          Map<Object, Object> map = getPeople().stream()
                .collect(Collectors.toMap(Person::getName, it -> helper()));
        return map;
    }
    public  static List<String> helper(){

        return  getPeople().stream()
                .map(Person::getAnimals)
                .flatMap(Collection::stream)
                .map(Animal::getName)
                .collect(Collectors.toList());
    }

    public  static  Optional <Animal> exampl10() {

        return  getPeople().stream()
                .map(Person::getAnimals)
                .flatMap(Collection::stream)
                .filter(it -> it.getAge() > 5)
                .findAny();
    }

    static class Cat implements Animal {

        private String name;
        private int age;

        public Cat(String name, int age) {
            this.name = name;
            this.age = age;
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public int getAge() {
            return age;
        }

        @Override
        public String voice() {
            return "Мяууу";
        }

        @Override
        public String toString() {
            return "Cat{" +
                    "name='" + name + '\'' +
                    ", age=" + age +
                    '}';
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Cat cat = (Cat) o;
            return age == cat.age &&
                    Objects.equals(name, cat.name);
        }

        @Override
        public int hashCode() {
            return Objects.hash(name, age);
        }
    }

    static class Dog implements Animal {
        private String name;
        private int age;

        public Dog(String name, int age) {
            this.name = name;
            this.age = age;
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public int getAge() {
            return age;
        }

        @Override
        public String voice() {
            return "Гав!";
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Dog dog = (Dog) o;
            return age == dog.age &&
                    Objects.equals(name, dog.name);
        }

        @Override
        public int hashCode() {
            return Objects.hash(name, age);
        }

        @Override
        public String toString() {
            return "Dog{" +
                    "name='" + name + '\'' +
                    ", age=" + age +
                    '}';
        }
    }
}



//    public  static  Map<Object, Object> exampl9() {
//        Person vito = new Person("Vito");
//        vito.getAnimals().add(new Dog("Sharik", 3));
//        vito.getAnimals().add(new Cat("Tom", 5));
//
//        Person michael = new Person("Michael");
//        michael.getAnimals().add(new Cat("Pushok", 1));
//        michael.getAnimals().add(new Dog("Barsik", 7));
//        michael.getAnimals().add(new Dog("Sharik", 5));
//
//        Person sarah = new Person("Sarah");
//        sarah.getAnimals().add(new Cat("Kiki", 3));
//        sarah.getAnimals().add(new Cat("Tom", 2));
//        sarah.getAnimals().add(new Dog("Piki", 10));
//        sarah.getAnimals().add(new Dog("Tiki", 8));
//
//        Collection<Person> people = Arrays.asList(vito, michael, sarah);
//
//        Map<Object, Object> map = people.stream()
//                .collect(Collectors.toMap(it -> it.getName(),
//                        it -> i.getAnimals())
//                        .flatMap(i -> i.stream())
//                        .map(i -> it.getName())));
//
//        return map;
//to do
//    }
//        1) Печатает список всех животных которые есть у всех людей
//
//        2) Возвращает коллекцию животных, которые есть у всех людей
//
//        3) Возвращает список всех собак
//
//        4) Возвращает среднее арифметическое возраста всех котов с именем НЕ Том
//
//        5) Вызывает метод voice у всех животных, которые старше 4 лет
//
//        6) Возвращает коллекцию людей у которых больше 2х животных
//
//        7) Возвращает коллекцию всех имен котов (без дубликатов), отсортированную
//
//        8) Возвращает коллекцию всех имен собак (без дубликатов), написанных большими буквами
//
//        9) Возвращает мапу в которой ключ - имя владельца, значение - массив имен его питомцев
//
//        10) Находит любого питомца с возрастом больше 5 лет
//

